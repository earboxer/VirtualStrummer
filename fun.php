<?php
$gitHash = exec("git rev-parse --short HEAD");
?>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=.65">
<meta name="theme-color" content="#000000">
<link rel="stylesheet" type="text/css" href="index.css?<?php echo $gitHash?>">
<link rel="stylesheet" type="text/css" href="fun.css?<?php echo $gitHash?>">
<!--
author: Zach DeCook
date: June 27, 2018
-->
</head>
<body>

Note: Requires a touchscreen<br/>

<div id=toucharea class='touch'>

<div class="oct" data-up="12"></div>

<div class="sev" data-up="10">7th</div>
<div class="six" data-up="9">6th</div>
<div class="Mj7" data-up="11">Mj7</div>
<!--<div class="nin">9</div>-->


<div class="dim" data-up="6">dim</div>
<div class="five" data-up="7">five</div>
<div class="aug" data-up="8">aug</div>

<div class="sus2" data-up="2">sus2</div>
<div class="min" data-up="3">min</div>
<div class="maj" data-up="4">maj</div>
<div class="sus4" data-up="5">sus4</div>

<div class="E4 mainNote">  E</div>
<div class="F4 mainNote">  F</div>
<div class="Gb4 mainNote"> F#</div>
<div class="G4 mainNote">  G</div>
<div class="Ab4 mainNote"> G#</div>
<div class="A4 mainNote">  A</div>
<div class="Bb4 mainNote"> Bb</div>
<div class="B4 mainNote">  B</div>
<div class="C5 mainNote">  C</div>
<div class="Db5 mainNote">C#</div>
<div class="D5 mainNote"> D</div>
<div class="Eb5 mainNote">D#</div>

<div class="E3 bassNote">  E</div>
<div class="F3 bassNote">  F</div>
<div class="Gb3 bassNote"> F#</div>
<div class="G3 bassNote">  G</div>
<div class="Ab3 bassNote"> G#</div>
<div class="A3 bassNote">  A</div>
<div class="Bb3 bassNote"> Bb</div>
<div class="B3 bassNote">  B</div>
<div class="C4 bassNote">  C</div>
<div class="Db4 bassNote">C#</div>
<div class="D4 bassNote"> D</div>
<div class="Eb4 bassNote">D#</div>

</div>

	Licensed under the <a href='LICENSE.md'>GNU AGPLv3</a>.
	View source <a href='source.php'>here</a>.
	<br>

	<script src="MIDI.js/inc/shim/Base64.js" type="text/javascript"></script>
	<script src="MIDI.js/inc/shim/Base64binary.js" type="text/javascript"></script>
	<script src="MIDI.js/inc/shim/WebAudioAPI.js" type="text/javascript"></script>
	<!-- midi.js package -->
	<script src="MIDI.js/js/midi/audioDetect.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/gm.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/loader.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/plugin.audiotag.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/plugin.webaudio.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/plugin.webmidi.js" type="text/javascript"></script>
	<!-- utils -->
	<script src="MIDI.js/js/util/dom_request_xhr.js" type="text/javascript"></script>
	<script src="MIDI.js/js/util/dom_request_script.js" type="text/javascript"></script>

	<script   src="https://code.jquery.com/jquery-1.12.4.min.js"
	integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
	crossorigin="anonymous"></script>
	<script src="fun.js?<?php echo $gitHash?>"></script>
</body>
</html>
