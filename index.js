/**
 * A program to allow playing a virtual guitar with a touchscreen.
 * Copyright (C) 2018 Zach DeCook
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
window.onload = function () {
	MIDI.loadPlugin({
		soundfontUrl: "MIDI.js/examples/soundfont/",
		instrument: "acoustic_grand_piano",
		onprogress: function(state, progress) {
			console.log(state, progress);
		},
		onsuccess: function() {
			// play the note
			MIDI.setVolume(0, 127);
		}
	});
};

// EADGBE - standard guitar tuning
var tuning = [40, 45, 50, 55, 59, 64];
// clone array
var note = tuning.slice(0);

/**
 * @brief Change the note array to play the most similar chord found in chordsDict
 */
function changeChord(chord){


	chord = chord.replace("(", "");
	chord = chord.replace(")", "");
	chord = chord.replace("sus", "s");
	chord = chord.replace("s4", "s");
	chord = chord.replace("s", "sus");
	chord = chord.replace("7sus", "sus7");
	chord = chord.replace("mj7", "maj7");
	chord = chord.replace("/A","/a");
	chord = chord.replace("/B","/b");
	chord = chord.replace("/C","/c");
	chord = chord.replace("/D","/d");
	chord = chord.replace("/E","/e");
	chord = chord.replace("/F","/f");
	chord = chord.replace("/G","/g");
	chord = chord.replace("Db", "C#");
	chord = chord.replace("Eb", "D#");
	chord = chord.replace("Gb", "F#");
	chord = chord.replace("Ab", "G#");
	chord = chord.replace("Bb", "A#");


	var tabSet = null;
	while( ! (tabSet = chordsDict[chord]) )
	{
		chord = chord.slice(0,-1)
		if ( chord == "" ) return false;
	}
	for(var i = 0; i < 6; i++){
		note[i] = -1;
		var position = parseInt(tabSet[0][i]);
		// Adding NaN to something will make it NaN, so the 'x's won't play.
		note[i] = tuning[i] + position;
	}
	updateAppearance(chord, tabSet[0]);
	return chord + " " + tabSet[0];
}

function updateAppearance(chord, tabset){
	document.getElementById('touchplay').innerHTML = chord;
}

window.addEventListener('load', function(){ // on page load

	var surface = document.getElementById('toucharea');
	var surface2 = document.getElementById('toucharea2');

	var X;
	var pX = 0;

	surface.addEventListener('touchstart', function(e){
		X = Math.round(e.changedTouches[0].pageX);
		pX = X;
	});

	surface2.addEventListener('touchstart', function(e){
		X = Math.round(e.changedTouches[0].pageX);
		pX = X;
	});
	surface2.addEventListener('touchend', muteThenNextChord );

	surface.addEventListener('touchmove', handleTouchMove, false);
	surface2.addEventListener('touchmove', handleTouchMove, false);

	function handleTouchMove(e){
		e.preventDefault(); // Prevent scrolling here.
		X = Math.round(e.changedTouches[0].pageX);

		// TODO: calculate velocity with distance and time.
		vel = 100;
		var width = surface2.clientWidth;
		for (var i = 0; i < 6; i++){
			strloc = (  (.15 * (i + 1)) * width  ) - (.02 * width);
			if( ( X > strloc && pX <= strloc ) || ( X < strloc && pX >= strloc) ){
				MIDI.noteOn(0, note[i], vel, 0);
				break;
			}
		}
		//string1.innerHTML = X;
		pX = X;
	}

	var touchplaynext = document.getElementById('touchplaynext')
	var touchplay = document.getElementById('touchplay')

	touchplaynext.addEventListener('touchstart', playChord, false );
	touchplay.addEventListener('touchstart', playChord, false );
	touchplaynext.addEventListener('touchend', muteThenNextChord, false );

	var touchprev = document.getElementById('touchprev')
	touchprev.addEventListener('touchstart', function(e){
		e.preventDefault();
		selectPreviousChord()
	}, false );
	var touchrestart = document.getElementById('touchrestart')
	touchrestart.addEventListener('touchstart', function(e){
		e.preventDefault();
		selectBeginning()
	}, false );
	var touchnext = document.getElementById('touchnext')
	touchnext.addEventListener('touchstart', function(e){
		e.preventDefault();
		selectNextChord()
	}, false );


}, false)

function playChord(e){
	e.preventDefault(); // Prevent scrolling here.
	for (var i = 0; i < 6; i++){
		MIDI.noteOn(0, note[i], 100, i * .01);
	}

	return false;
}
function nothing(e){
	e.preventDefault();
}
function muteThenNextChord(e){
	e.preventDefault();
	for (var i = 0; i < 6; i++){
		MIDI.noteOff(0, note[i], 0, 0);
	}
	selectNextChord();
}
