/**
 * Follow along with the chords on a page
 * Copyright (C) 2018 Zach DeCook
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

function getRange(selection){
	if ( selection.anchorNode ){
		return selection.getRangeAt(0);
	}
	else {
		// Creating new range (Safari support).
		var range = document.createRange();
		if( selection.anchorNode ){
			range.setStart( selection.anchorNode, 0 )
			range.setEnd( selection.anchorNode, 0 )
		}
		return range;
	}
}

function placeRange(s,r){
	if ( navigator.userAgent.indexOf("Safari") != -1 ){
		// Safari support
		s.removeAllRanges();
		s.addRange(r);
	}
}

function handleTabsClick(e){
	s = window.getSelection();
	var range = getRange(s)
	var node = s.anchorNode;
	while (range.toString().indexOf(' ') != 0 ) {
		if (range.startOffset == 0)
			break;
		range.setStart(node, (range.startOffset - 1));
    }
	while (range.toString().lastIndexOf(' ') != range.toString().length - 1
		|| range.toString().length <= 1 )
	{
		if ( range.endOffset == node.length)
			break;
		range.setEnd(node, range.endOffset+1);
    }
    var str = range.toString().trim();
	if( str != "" )
		changeChord(str);
}


function selectBeginning(){
	var pre = document.getElementsByTagName('pre')[0];
	var tab = document.getElementsByClassName('tabs')[0];
	var selection = window.getSelection();
	var range = getRange(selection)
	console.log(tab.firstChild)
	range.setStart(tab.firstChild,0)
	range.setEnd(tab.firstChild,0)
	placeRange(selection, range);
}

function selectNextChord(depth = 2){
	s = window.getSelection();
	var range = getRange(s);
	var node = s.anchorNode;
	range.setStart(node, range.endOffset);
	if ( val = selectInExtendedRange(range) ){
		placeRange(s, range);
		return changeChord(val);
	}
	else if (depth > 0){
		n2 = node.parentElement.nextElementSibling.firstChild
		if ( n2 ){
			range.setStart(n2,0)
			range.setEnd(n2,0)
			placeRange(s, range);
			return selectNextChord(depth - 1);
		}
	}
	return false;
}

function selectPreviousChord(depth = 2){
	s = window.getSelection();
	var range = getRange(s);
	var node = s.anchorNode;
	range.setEnd(node, range.startOffset);
	if ( val = selectInPreviousRange(range) ){
		placeRange(s, range);
		return changeChord(val);
	}
	else if (depth > 0){
		n2 = node.parentElement.previousElementSibling.firstChild;
		if ( n2 ){
			range.setStart(n2,n2.length)
			range.setEnd(n2,n2.length)
			return selectPreviousChord(depth - 1);
		}
	}
	return false;
}

function selectInExtendedRange(range){
	s = window.getSelection();
	var node = s.anchorNode;

	while( range.endOffset < node.length ){
		range.setEnd(node, range.endOffset+1);
		str = range.toString();
		if ( str.trim() == '' ){
			continue
		}
		if ( str.lastIndexOf(' ') == range.toString().length - 1 ){
			break;
		}
	}
	while( range.toString().indexOf(' ') == 0 ){
		range.setStart(node, range.startOffset + 1);
	}
	return range.toString().trim();
}

function selectInPreviousRange(range){
	s = window.getSelection();
	var node = s.anchorNode;

	while( range.startOffset > 0 ){
		range.setStart(node, range.startOffset - 1);
		str = range.toString();
		if ( str.trim() == '' ){
			continue
		}
		if ( str.indexOf(' ') == 0 ){
			break;
		}
	}
	while( range.toString().lastIndexOf(' ') == range.toString().length - 1
		&& range.endOffset != 0){
		range.setEnd(node, range.endOffset - 1);
	}
	return range.toString().trim();
}

window.addEventListener('load', function(){ // on page load
	var tabs = document.getElementsByClassName('tabs');
	for (i = 0, len = tabs.length; i < len; i++){
		tabs[i].addEventListener('click', handleTabsClick );
	}
	selectBeginning();
});
