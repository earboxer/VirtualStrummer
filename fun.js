var lastel = '';
var playing = [];

$(function(){
	MIDI.loadPlugin({
		soundfontUrl: "MIDI.js/examples/soundfont/",
		instrument: "acoustic_grand_piano",
		onprogress: function(state, progress) {
			console.log(state, progress);
		},
		onsuccess: function() {
			// play the note
			MIDI.setVolume(0, 127);
		}
	});
	$('#toucharea').bind('touchstart', function(e){
		lastel = '';
		funfun(e);
	});
	$('#toucharea').bind('touchmove', function(e){
		funfun(e);
	});
	$('#toucharea').bind('touchend', unfun );
});

function funfun(e)
{
	e.preventDefault();
	var X = e.originalEvent.touches[0].pageX
	var Y = e.originalEvent.touches[0].pageY
	var touch = e.changedTouches
	//console.log(touch);
	var el = document.elementFromPoint(X,Y);
	if ( el != lastel)
	{
		var num = -1;
		if ((num = MIDI.keyToNote[el.className.match(/[^ ]*/)[0]]))
		{
			if ( el.className.match(/mainNote/) )
			{
				if ( playing.push(num) )
				{

					playing.push(num);
				}
				MIDI.noteOn(0, playing[0], 100, 0);
			}
			else
			{
				MIDI.noteOn(0, num, 100, 0);
			}
		}
		else if ( jQuery(el).attr('data-up') )
		{
			num = playing[0] + parseInt(jQuery(el).attr('data-up'));
			MIDI.noteOn(0, num, 100, 0);
			playing.push(num);
		}
	}
	lastel = el;
}

function unfun(e)
{
	e.preventDefault();
	var num = -1;
	while( num = playing.pop() )
	{
		MIDI.noteOff(0, num, 100, 0);
	}
	return 'ok';
}
