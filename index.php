<?php
$gitHash = exec("git rev-parse --short HEAD");
?>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=.65">
<meta name="theme-color" content="#000000">
<link rel="stylesheet" type="text/css" href="index.css?<?php echo $gitHash?>">
<!--
author: Zach DeCook
date: June 8, 2018
-->
</head>
<body>

<form action='#' onsubmit="$('pre').load(jQuery( 'input' ).val() + ' pre');">
<input name='source' placeholder='enter URL' ></input>
</form>
<a href='#' onclick='selectPreviousChord()'> < </a>
<a href='#' onclick='selectBeginning()'> ^ </a>
<a href='#' onclick='selectNextChord()'> > </a>
<br/><a href='#' onclick='muteThenNextChord(event);playChord(event);'> |> </a>

<pre>
</pre>

Note: Requires a touchscreen<br/>

<div id='touchprev' class='touch'> <
</div><div id='touchrestart' class=touch>^</div><div id='touchnext' class='touch'> > </div>
<div id='touchplay' class=touch>_</div><div id='touchplaynext' class=touch>_</div>


<div id=toucharea class='touch'>

<div class="string1"></div>
<div class="string2"></div>
<div class="string3"></div>
<div class="string4"></div>
<div class="string5"></div>
<div class="string6"></div>

</div>
<div id='toucharea2' class='touch'>

<div class="string1"></div>
<div class="string2"></div>
<div class="string3"></div>
<div class="string4"></div>
<div class="string5"></div>
<div class="string6"></div>

</div>

	Licensed under the <a href='LICENSE.md'>GNU AGPLv3</a>.
	View source <a href='source.php'>here</a>.
	<br>

	<script src="MIDI.js/inc/shim/Base64.js" type="text/javascript"></script>
	<script src="MIDI.js/inc/shim/Base64binary.js" type="text/javascript"></script>
	<script src="MIDI.js/inc/shim/WebAudioAPI.js" type="text/javascript"></script>
	<!-- midi.js package -->
	<script src="MIDI.js/js/midi/audioDetect.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/gm.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/loader.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/plugin.audiotag.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/plugin.webaudio.js" type="text/javascript"></script>
	<script src="MIDI.js/js/midi/plugin.webmidi.js" type="text/javascript"></script>
	<!-- utils -->
	<script src="MIDI.js/js/util/dom_request_xhr.js" type="text/javascript"></script>
	<script src="MIDI.js/js/util/dom_request_script.js" type="text/javascript"></script>

	<script src="chordsdata/chords.js"></script>
	<script src="index.js?<?php echo $gitHash?>"></script>
	<script src="follow.js?<?php echo $gitHash?>"></script>
	<script   src="https://code.jquery.com/jquery-1.12.4.min.js"
	integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
	crossorigin="anonymous"></script>
	<script>
	<?php $src = $_GET['source'] ?? 'grace.html'; ?>
	$('pre').load("<?php echo $src; ?> pre");
	$('input').val("<?php echo $src;?>");
	</script>
</body>
</html>
